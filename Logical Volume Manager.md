# Logical Volume Manager

## Introduction
LVM is abbreviation of Logical Volume Manager. It is a tool for Logical Volume Management. It manages storage of a system. LMV is made of four conceptual layers physical volume, volume group, logical volume and file system. 

## Why use LVM?
LVM can be thought as 'dynamic partitions'. LVM partitions can be created/ updated/ deleted through terminal while system is online. There is no need to reboot system in order to let kernel aware of new/updated partitions. 

Logical Volume can be extended to multiple disks. Logical Volume is not restricted by the available size in single disks, rather on volume can expand into multiple disks' available storage.

Snapshot of any Logical Volume can be created to revert back to its stable version. Snapshots can be deleted if no longer needed. 
