# User Management

## Introduction:
**A user** in linux operating system can be considered as an entity that can perform operations and manupulate files and I/O. Users can also be an interface in Operating System that lets an end user with a keyboard or some other I/O hardware access its features and control the Hardware based on the privileges it has. 

`cat /etc/passwd` to list users. Alternatively `getent passwd` can be used.

![Image](./media/User Management user list.jpg "user list")

`id username` outputs uid, gid and groups information of that user.

```bash
$ id custom

$ uid=1005(custom) gid=1003(custom) groups=1003(custom),1002(ritwik),1004(ritwik2)
```


Format of user info: 
username:password:uid:gid:comments:home_directory:shell

**x** in place of password means `encrypted password` is stored in `shadow file`. root account always have UID 0. System UID ranges from `100 to 999` and custom user UID ranges from `1000 to 60000` as configured in `/etc/login.defs`. Although a user can be in multiple groups, but GID in passwd file is the default group GID of user.

Shell mentioned for each user will be executed when a user logsin. For **root** user, bash shell will be exeucted. Few users in the list has `/sbin/nologin` and `/bin/false` as shell. It prevents use of those users. **Example:** daemon user was not meant to be accessed directly rather it is used for running applications under it so that its access can be restricted. 

**Group** is a collection of users. Groups and users has many-to-many relationship. Groups can also have permissions like users. Instead of giving permissions to individual users, a group with needed permissions can be created and users can be added to groups. `cat /etc/group` to list groups. Alternatively `getent group` can be used.

![Image](./media/User Management group list.jpg "group list")

## Commands
#### Add a new User
**useradd** creates a new user into system. `useradd -c "Ritwik Math" -m -s /bin/sh ritwik` creates a new user `ritwik` with comment `Ritwik Math` and default shell `/bin/sh`. List of available shells can be found in `/etc/shells`.  

![Image](./media/User Management user create.jpg "user create")

**-g** assigns a default group to user. If not mentioned then system creates a new group with same user name. 

![Image](./media/User Management user add default group.jpg "user create default sudo group")

**-G** to add user to multiple additional groups. 

![Image](./media/User Management user add multiple groups.png "user create default sudo group")

New user has default `GID 0` which is for root.

**-u** creates a new user with provided user id.

![Image](./media/User Management user add specififc id.png "password change")

**passwd** assign a password to user. `passwd ritwik` to assign password. 

![Image](./media/User Management password.jpg "password change")

**-s** assign a default shell to user. List available shells `cat /etc/shells`.

![Image](./media/User Management user create shell.png "default shell")

**-r** creates a system account.When an account is needed to run daemon, system software or service, system account should be created. `sudo useradd -r -d /snap/bin/documentnode -s /bin/bash noderitwik` creates a new user **noderitwik** as a system user and it's home directory is an application's installation directory. User gets an UID `999` and default group is `noderitwik`.

#### Update a user
**usermod** updates a user account. Almost all options used in useradd can be used in usermod. **-aG** append user to more groups.

![Image](./media/User Management user update.png "user update")


#### Delete an existing user
**userdel** deletes an existsing user from system. `userdel noderitwik`, when executed, system deletes user info from `/etc/passwd` and `/etc/group`.

**-r** deletes home directory and mail spool of user. Only `userdel` does not remove mail spool and home directory from system.

**killall** kills all the user processes in case any process of that particualr user is running, `killall -u ronnie`

**-f** forcefully removes a user from system, `userdel -f ronnie`.

#### Add a new Group
**groupadd** adds a new group in system. `groupadd custom` adds a new group named *custom* in system. 

**-g** specifies the group id will be assigned to the new group. `groupadd -g 1004 custom` will create a group with `GID 1004`.

**-o** creates a group with existing GID.

![Image](./media/User Management group add with non-unique id.png "default shell")

#### Delete an existing group
**groupdel** deletes a group. But it cannot delete a group if it is primary group of a user. 

**-f** it deletes a group forcefully. If deleted forcefully, user will have no defaut group. 

![Image](./media/User Management group delete forcefully.png "group delete forcefully")